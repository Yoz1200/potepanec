require 'rails_helper'

RSpec.feature 'Potepan::Categories', type: :feature do
  let!(:option_type_color) do
    create(:option_type,  name: 'shirt-color',
                          presentation: 'Color')
  end
  let!(:option_type_size) do
    create(:option_type,  name: 'shirt-size',
                          presentation: 'Size')
  end
  let!(:color_red) do
    create(:option_value, name: 'red',
                          presentation: 'Red',
                          option_type: option_type_color)
  end
  let!(:color_blue) do
    create(:option_value, name: 'blue',
                          presentation: 'Blue',
                          option_type: option_type_color)
  end
  let!(:color_yellow) do
    create(:option_value, name: 'yellow',
                          presentation: 'Yellow',
                          option_type: option_type_color)
  end
  let!(:size_s) do
    create(:option_value, name: 'size-s',
                          presentation: 'S',
                          option_type: option_type_size)
  end
  let!(:size_m) do
    create(:option_value, name: 'size-m',
                          presentation: 'M',
                          option_type: option_type_size)
  end
  let!(:size_l) do
    create(:option_value, name: 'size-l',
                          presentation: 'L',
                          option_type: option_type_size)
  end

  let!(:taxonomy) { create(:taxonomy, name: 'Category') }
  let!(:clothing) { taxonomy.root.children.create(name: 'Clothing') }
  let!(:shirts) { clothing.children.create(name: 'Shirts', taxonomy: taxonomy) }
  let!(:good_shirts) do
    create(:product,  name: 'Good-Shirt',
                      description: 'High quality',
                      price: 900,
                      available_on: 6.days.ago,
                      taxons: [shirts],
                      option_types: [option_type_color, option_type_size])
  end
  let!(:bad_shirts) do
    create(:product,  name: 'Bad-Shirt',
                      description: 'Low quality',
                      price: 100,
                      available_on: 2.days.ago,
                      taxons: [shirts],
                      option_types: [option_type_color, option_type_size])
  end
  let!(:normal_shirts) do
    create(:product,  name: 'Normal-Shirt',
                      price: 500,
                      available_on: 4.days.ago,
                      taxons: [shirts],
                      option_types: [option_type_color, option_type_size])
  end

  let!(:good_shirts_red_s) do
    create(:variant,  product: good_shirts,
                      option_values: [color_red, size_s])
  end
  let!(:bad_shirts_blue_m) do
    create(:variant,  product: bad_shirts,
                      option_values: [color_blue, size_m])
  end
  let!(:normal_shirts_red_m) do
    create(:variant,  product: normal_shirts,
                      option_values: [color_red, size_m])
  end

  let!(:bag)  { taxonomy.root.children.create(name: 'Bag') }
  let!(:tote) { bag.children.create(name: 'Tote', taxonomy: taxonomy) }
  let!(:good_tote) do
    create(:product,  name: 'Good-Tote',
                      taxons: [tote])
  end

  before do
    visit potepan_category_path(shirts.id)
  end

  scenario 'タイトルが正しく表示されていること' do
    expect(page).to have_title "#{shirts.name} - BIGBAG Store"
  end

  scenario '商品カテゴリーが正しく表示されていること' do
    node = first(:css, 'div.panel-body')
    within node do
      expect(page).to have_content('Category')
      expect(page).to have_content('Shirts (3)')
      expect(page).to have_content('Tote (1)')
    end
  end

  scenario '「色から探す」が正しく表示されていること' do
    node = all(:css, 'div.panel-body')[1]
    within node do
      expect(page).to have_content('Red(2)')
      expect(page).to have_content('Blue(1)')
      expect(page).to have_content('Yellow(0)')
    end
  end

  scenario '「サイズから探す」が正しく表示されていること' do
    node = all(:css, 'div.panel-body')[2]
    within node do
      expect(page).to have_content('S(1)')
      expect(page).to have_content('M(2)')
      expect(page).to have_content('L(0)')
    end
  end

  scenario '商品が正しく表示されていること' do
    within first('.productCaption') do
      expect(page).to have_content bad_shirts.name
      expect(page).to have_content bad_shirts.display_price
    end
  end

  scenario '商品一覧から商品ページへ移動すること' do
    click_on bad_shirts.name
    expect(current_path).to eq potepan_product_path(bad_shirts.id)
  end
end
