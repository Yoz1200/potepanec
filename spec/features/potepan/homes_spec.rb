require 'rails_helper'

RSpec.feature 'Potepan::Homes', type: :feature do
  let!(:popular_taxon) { create(:taxon, name: 'Bags') }
  let!(:taxon) { create(:taxon) }
  let!(:new_product) { create(:product, taxons: [taxon]) }
  let!(:recent_arrival) do
    create(:product, name: 'RecentArraival',
                     price: 50.00,
                     available_on: 2.days.ago, taxons: [taxon])
  end
  let!(:new_arrival) do
    create(:product, name: 'NewArraival',
                     price: 100.00,
                     available_on: 14.days.ago, taxons: [taxon])
  end
  let!(:old_arrival) do
    create(:product, name: 'OldArraival',
                     price: 200.00,
                     available_on: 15.days.ago, taxons: [taxon])
  end

  before do
    visit potepan_path
  end

  scenario 'タイトルがサイト名となっていること' do
    expect(page).to have_title('BIGBAG Store')
  end

  scenario 'タイトルロゴからrootへ移動すること' do
    click_on 'title_logo'
    expect(current_path).to eq potepan_path
  end

  scenario 'navbarの"HOME"からrootへ移動すること' do
    click_on 'HOME'
    expect(current_path).to eq potepan_path
  end

  scenario '人気カテゴリーからカテゴリーページへ移動すること' do
    expect(page).to have_link popular_taxon.name
    click_on popular_taxon.name
    expect(current_path).to eq potepan_category_path(popular_taxon.id)
  end

  scenario '2日前の新着商品が正しく表示されていること' do
    expect(page).to have_content recent_arrival.name
    expect(page).to have_content recent_arrival.display_price
  end

  scenario '2週間前の新着商品が正しく表示されていること' do
    expect(page).to have_content new_arrival.name
    expect(page).to have_content new_arrival.display_price
  end

  scenario '2週間+1日前の新着商品は表示されないこと' do
    expect(page).to have_no_content old_arrival.name
    expect(page).to have_no_content old_arrival.display_price
  end

  scenario '新着商品から商品ページへ移動すること' do
    expect(page).to have_link recent_arrival.name
    click_on recent_arrival.name
    expect(current_path).to eq potepan_product_path(recent_arrival.id)
  end
end
