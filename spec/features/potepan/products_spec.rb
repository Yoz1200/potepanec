require 'rails_helper'

RSpec.feature 'Potepan::Products', type: :feature do
  let!(:taxonomy) { create(:taxonomy) }
  let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario 'タイトルが正しく表示されていること' do
    expect(page).to have_title("#{product.name} - BIGBAG Store")
  end

  scenario 'タイトルロゴからrootへ移動すること' do
    click_on 'title_logo'
    expect(current_path).to eq potepan_path
  end

  scenario 'navbarの"HOME"からrootへ移動すること' do
    click_on 'HOME'
    expect(current_path).to eq potepan_path
  end

  scenario 'ページタイトルに商品名が表示されていること' do
    within '.page-title' do
      expect(page).to have_content product.name
    end
  end

  scenario '商品詳細ページが正しく表示されること' do
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.description
      expect(page).to have_content product.display_price
    end
  end

  scenario '関連商品が正しく表示されていること' do
    within '.productsContent' do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
    end
  end

  scenario '関連商品から商品ページに移動すること' do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end

  scenario '一覧ページへ戻るをクリックしたら一覧ページへ戻れること' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario 'カートへ入れるボタンが表示されていること' do
    expect(page).to have_button 'カートへ入れる'
  end

  scenario 'トップバーからカートへ移動すること' do
    find('#shopping-cart').click
    expect(current_path).to eq potepan_cart_path
  end
end
