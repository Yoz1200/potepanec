require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let!(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
    let!(:taxonomy) { create(:taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end
    it 'assigns :taxon' do
      expect(assigns(:taxon)).to eq taxon
    end
    it 'assigns :taxonomy' do
      expect(assigns(:taxonomies)).to include taxonomy
    end
    it 'assigns :product' do
      expect(assigns(:products)).to include product
    end
    it 'returns http success' do
      expect(response).to be_successful
    end
    it 'render a template :show' do
      expect(response).to render_template :show
    end
  end
end
