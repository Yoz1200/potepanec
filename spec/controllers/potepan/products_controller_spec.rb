require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let!(:product) { create(:product) }
    before do
      get :show, params: { id: product.id }
    end
    it 'assigns :product' do
      expect(assigns(:product)).to eq product
    end
    it 'returns http success' do
      expect(response).to be_successful
    end
    it 'render a template :show' do
      expect(response).to render_template :show
    end
  end
end
