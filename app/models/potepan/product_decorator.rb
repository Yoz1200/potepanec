module Potepan::ProductDecorator
  #===== クラスメソッド =====
  def self.prepended(base)
    def base.new_arrival_from(from)
      where(available_on: from..Time.zone.today)
        .order(available_on: 'DESC')
        .includes(master: %i[default_price images])
    end
  end

  #===== インスタンスメソッド =====
  def extract_related_products
    Spree::Product.in_taxons(taxons)
                  .includes(master: %i[default_price images])
                  .where.not(id: id)
                  .distinct
  end
  Spree::Product.prepend self
end
