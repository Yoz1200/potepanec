module Potepan::TaxonDecorator
  #===== インスタンスメソッド =====
  def products_sort(sort_type)
    Spree::Product
      .joins(:taxons).where(spree_taxons: { id: self_and_descendants.ids })
      .includes(:variants, master: %i[images default_price])
      .eager_load(variants: %i[prices])
      .distinct
      .order(order_sentence(sort_type))
  end

  def order_sentence(sort_type)
    case sort_type
    when 'NEW_PRODUCTS'
      'spree_products.available_on DESC'
    when 'OLD_PRODUCTS'
      'spree_products.available_on ASC'
    when 'LOW_PRICE'
      'default_prices_spree_variants.amount ASC'
    when 'HIGH_PRICE'
      'default_prices_spree_variants.amount DESC'
    end
  end

  Spree::Taxon.prepend self
end
