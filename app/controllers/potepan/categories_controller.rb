class Potepan::CategoriesController < ApplicationController
  DEFAULT_SORT = 'NEW_PRODUCTS'
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(:root)
    @colors = Spree::OptionValue.joins(:option_type).where('spree_option_types.presentation like ?', '%color%')
    @sizes = Spree::OptionValue.joins(:option_type).where('spree_option_types.presentation like ?', '%size%')
    @colors_info = {}
    @colors.each do |color|
      value = Spree::Product.in_taxon(@taxon)
                            .joins(variants: :option_values)
                            .where(spree_option_values: { name: color.name })
                            .distinct
                            .count
      @colors_info[color.name.to_sym] = value
    end
    @sizes_info = {}
    @sizes.each do |size|
      value = Spree::Product.in_taxon(@taxon)
                            .joins(variants: :option_values)
                            .where(spree_option_values: { name: size.name })
                            .distinct
                            .count
      @sizes_info[size.name.to_sym] = value
    end
    @products = if params[:sort]
                  @taxon.products_sort(params[:sort])
                elsif params[:color]
                  Spree::Product.in_taxon(@taxon)
                                .includes(master: %i[default_price images])
                                .includes(variants: :option_values)
                                .where(spree_option_values: { name: params[:color] })
                elsif params[:size]
                  Spree::Product.in_taxon(@taxon)
                                .includes(master: %i[default_price images])
                                .includes(variants: :option_values)
                                .where(spree_option_values: { name: params[:size] })
                else
                  @taxon.products_sort(DEFAULT_SORT)
                end
  end
end
