class Potepan::OrdersController < ApplicationController
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Store
  include Spree::Core::ControllerHelpers::Auth

  def edit
    @order =
      current_order ||
      Spree::Order.incomplete.find_or_initialize_by(
        guest_token: cookies.signed[:guest_token]
      )
  end

  def update
    @order = Spree::Order.find(params[:id])
    @order.contents.update_cart(order_params)
    redirect_to potepan_cart_path
  end

  def remove_line_item
    line_item = Spree::LineItem.find(params[:id])
    line_item.order.contents.remove_line_item(line_item)
    redirect_to potepan_cart_path
  end

  def populate
    @order = current_order(create_order_if_necessary: true)
    authorize! :update, @order, cookies.signed[:guest_token]

    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].present? ? params[:quantity].to_i : 1

    begin
      @line_item = @order.contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => e
      @order.errors.add(:base, e.record.errors.full_messages.join(', '))
    end

    if @order.errors.any?
      flash[:error] = @order.errors.full_messages.join(', ')
      redirect_back_or_default(potepan_path)
      return
    else
      redirect_to potepan_cart_path
    end
  end

  private

  def order_params
    params.require(:order).permit(line_items_attributes: %i[id quantity])
  end
end
