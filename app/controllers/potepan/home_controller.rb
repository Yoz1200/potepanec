class Potepan::HomeController < ApplicationController
  POPULAR_TAXONS = ["shirts", "bags", "mugs"].freeze
  def index
    @new_arrival_products = Spree::Product.new_arrival_from(2.weeks.ago) ||
                            Spree::Product.includes(master: %i[default_price images])
                                          .order(available_on: 'DESC')
    @popular_taxons = Spree::Taxon.where(name: POPULAR_TAXONS)
  end
end
