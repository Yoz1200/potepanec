class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_DISPLAY_MAX = 4
  def show
    @product = Spree::Product.find(params[:id])
    @images  = @product.images
    @related_products = @product.extract_related_products
                                .limit(RELATED_PRODUCTS_DISPLAY_MAX)
    @taxon = @product.taxons.first
  end

  def search
    @products = if params[:search]
                  Spree::Product.includes(master: %i[default_price images])
                                .where(['name LIKE ? OR description LIKE ?', "%#{params[:search]}%", "%#{params[:search]}%"])
                else
                  Spree::Product.includes(master: %i[default_price images])
                end
  end
end
