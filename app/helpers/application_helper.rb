module ApplicationHelper
  def full_page_title(title)
    base_title = 'BIGBAG Store'
    if title.blank?
      base_title
    else
      "#{title} - #{base_title}"
    end
  end
end
